function flattenTree(tree, config,expanded,searchModeTree) {
  const forest = Array.isArray(tree) ? tree : [tree]
  const list = walkNodes({ nodes: forest }, config,expanded,searchModeTree)
  return list
}

function walkNodes({ nodes, list = new Map(), parent, depth = 0 }, config,expanded,searchModeTree) {

  nodes.forEach((node, i) => {
    if (node) {
      if (searchModeTree) {
        node._depth = 0
      }
      else {
        node._depth = depth
      }

      // if (config.label != "label") {
      //   node["label"] = node[config.label]
      // }


      if (searchModeTree) {
        node["_noToggle"] = true
      }
      else {
        node["_noToggle"] = false
      }
      if (parent) {
        node._id = node[config.id] || `${parent._id}-${i}`
        node._parent = parent._id
        node.expanded = expanded
        parent._children.push(node._id)
      } else {
        node._id = node[config.id] || `${i}`
      }

      list.set(node._id, node)
      if (node[config.children]) {
        node._children = []
        walkNodes({ nodes: node[config.children], list, parent: node, depth: depth + 1 }, config)
        delete node.children
      }

    }
  })
  return list
}

export default flattenTree
