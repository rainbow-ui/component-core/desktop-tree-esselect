export class SelectUtil {
    static setSelectComponentValue(len, id) {
        if (len > 0) {
            $('#' + id).val(len)
        }
        else {
            $('#' + id).val('')
        }
    }
}
