import { Component } from 'rainbowui-desktop-core'
import PropTypes from 'prop-types'
import cn from 'classnames/bind'
import styles from '../../css/tag.css'

const cx = cn.bind(styles)

class Button extends Component {
  static propTypes = {
  }

  render() {
    return (
      <button onClick={this.onClick} className={cx('tag-remove')} type='button'>x</button>
    )
  }

  onClick = (e) => {
    // this is needed to stop the drawer from closing
    e.stopPropagation()
    this.props.onDelete(this.props.id, this.props.tagObj)
  }
}

export default class Tag extends Component {

  render() {
    const { id,node, label, onDelete, tagObj } = this.props;
    return (
      this.props.enabledDrop ?
      <span className={cx('tag')}>
        {label}
      </span>
      :
      <span className={cx('tag')}>
        {label}
        <Button id={id} onDelete={onDelete} tagObj={tagObj}  />
      </span>
    )
  }
}


/**
 * Button component prop types
 */
Button.propTypes = $.extend({}, Component.propTypes, {
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  tagObj: PropTypes.object
});

/**
 * Get Button component default props
 */
Button.defaultProps = $.extend({}, Component.defaultProps, {
});
/**
 * Tag component prop types
 */
Tag.propTypes = $.extend({}, Component.propTypes, {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  tagObj: PropTypes.object
});

/**
 * Get Tag component default props
 */
Tag.defaultProps = $.extend({}, Component.defaultProps, {
});