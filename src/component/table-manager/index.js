export default class TableManager {
    constructor(dataTable) {
        this.dataTable = dataTable
    }


    getTags(ids, config) {
        let result = []
        this.dataTable.forEach(element => {
            if (ids.includes(element[config["idCol"]])) {
                result.push(element);
            }
        });

        return result
    }

}
