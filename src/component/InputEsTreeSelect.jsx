/*!
 * React Dropdown Tree Select
 * A lightweight, fast and highly customizable tree select component.
 * Hrusikesh Panda <hrusikesh.panda@dowjones.com>
 * Copyright (c) 2017 Dow Jones, Inc. <support@dowjones.com> (http://dowjones.com)
 * license MIT
 * see https://github.com/dowjones/react-dropdow-tree-select
 */
import { UIInput, UISwitch, Param, UIMessageHelper } from 'rainbowui-desktop-core'
import { ValidatorContext } from "rainbow-desktop-cache";
import PropTypes, { instanceOf } from 'prop-types'
import cn from 'classnames/bind'
import Dropdown from './Dropdown'
import DropdownTrigger from './DropdownTrigger'
import DropdownContent from './DropdownContent'
import { Util } from 'rainbow-desktop-tools';
import TreeManager from './tree-manager'
import Tree from './tree'
import { TreeInput } from './input'
import { SelectUtil } from './select-utils.js'
import styles from '../css/index.css'
import flattenTree from './tree-manager/flatten-tree'

const cx = cn.bind(styles)

export default class InputEsTreeSelect extends UIInput {


    constructor(props) {
        super(props)

        this.state = {
            dropdownActive: this.props.showDropdown || false,
            searchModeOn: false,
            inputLiClass: 'inputClass',
            componentValue: '',
            selectValue: [],
            searchTreeMode: false,
            searchKey: '',
            treeData:[],
            tree: null,
            tags:[],
            defaultTags: []
        }

        if (this.props.io == "out") {
            this.props.readOnly = true
        }

        this.storeNodeMap = new Map();

        this.onInputChange = this.onInputChange.bind(this)
        this.onDrowdownHide = this.onDrowdownHide.bind(this)
        if (!Util.parseBool(this.props.readOnly)) {
            this.onCheckboxChange = this.onCheckboxChange.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
        this.notifyChange = this.notifyChange.bind(this)
        this.onNodeToggle = this.onNodeToggle.bind(this)
    }

    notifyChange(...args) {
        // this.validatorValuePut(...args)
        var tempArgs = [...args];
        typeof this.props.onChange === 'function' && this.props.onChange(...args)
        if (this.props.model && this.props.property) {
            let propertyArr = this.props.model[this.props.property]
            if (propertyArr) {
                if (propertyArr instanceof Array) {
                    if (propertyArr.length > 0) {
                        $('#' + this.componentId).val('hasValue')
                    }
                    else {
                        $('#' + this.componentId).val('')
                    }
                }
            }
        }
    }




    loadESData(url, props, isMount) {

        let self = this
        let params = {};
        let propsRootNode = props.rootNode;
        if (!(propsRootNode instanceof Array)) {
            propsRootNode = [props.rootNode]
        }

        params["CodeTableName"] = props.CodeTableName
        params["PageSize"] = 100,
        params["PageNo"] = 0,

        params["ConditionMap"] = { "parentOrgId": propsRootNode && propsRootNode.length > 0 ? propsRootNode[0][props.jsonConfig.id] : ''}

        AjaxUtil.call(url, params, { "method": "POST" }).then((data) => {
            if (data) {
                let treeData = self.createList(data.BusinessCodeTableValueList)
                let tree = flattenTree(_.cloneDeep(treeData), self.props.jsonConfig)
                self.tree = tree
                self.testMount(tree, isMount)
                self.props.refreshSign =false
                self.setState({treeData: treeData, tree: tree})
            }
        });
        this.clearValidationInfo();
    }

    clearValidationInfo() {
        const self = this;
        const selectObj = $("#" + self.componentId);
        let formGroupObj = selectObj.closest(".form-group");
        if (formGroupObj.hasClass('has-feedback') || formGroupObj.hasClass('has-error')) {
            formGroupObj.removeClass('has-feedback');
            formGroupObj.removeClass('has-error');
            let removeIcon = selectObj.next();
            let errorMessage = selectObj.parent().next();
            if (errorMessage.length == 0) {
                errorMessage = selectObj.parent();
            }
            if (removeIcon && removeIcon.hasClass("form-control-feedback")) {
                removeIcon.remove();
            }
            if (errorMessage && errorMessage.hasClass("help-block")) {
                errorMessage.remove();
            }
            if ($(errorMessage[0]).children('small')) {
                $(errorMessage[0]).children('small').remove()
            }
        }
    }

    testMount(tree, isMount) {
        if (tree.size) {
            if (isMount) {
                if (!_.isEmpty(this.props.defaultValue)) {
                    if (!(this.props.model[this.props.property] == null)) {
                        this.props.model[this.props.property] = []
                    }
                    this.storeNodeMap.clear()

                    let arrIds = _.uniq(this.props.defaultValue.join(',').split(','));
                    let params = { "CodeTableName": this.props.CodeTableName, Ids: arrIds };
                    //load一波数据然后装进去
                    AjaxUtil.call(this.props.getTagsUrl, params, { "method": "POST" }).then((data) => {
                        if (data) {
                            tree.forEach(function(node) {
                                node.checked =false
                            }, this);
                            data.forEach(function (ele) {
                                let node = tree.get(ele[this.props.jsonConfig.id])
                                if (node) {
                                    node.checked = true
                                }
                                if (this.props.propSaveConentType == 'node') {
                                    ele._id = ele[this.props.jsonConfig.id]
                                    this.storeNodeMap.set(ele[this.props.jsonConfig.id], ele)
                                    this.props.model[this.props.property].push(ele)
                                } else {
                                    let theId = ele[this.props.jsonConfig.id]
                                    ele._id = ele[this.props.jsonConfig.id]
                                    this.storeNodeMap.set(theId, ele)
                                    this.props.model[this.props.property].push(theId)
                                }
                            }, this);
                        }

                        let tags = []
                        this.storeNodeMap.forEach(function (ele) {
                                tags.push(ele)
                            let tempNode = tree.get(ele._id)
                            if (tempNode) {
                                tempNode.checked = true
                            }

                        }, this);
                        if(tags.length >0){

                        $('#' + this.componentId).val('hasValue')
                    }
                    else{
                        $('#' + this.componentId).val('')
                    }

                        this.props.refreshSign = true
                        this.setState({ tree: tree, tags: tags })
                        this.props.refreshSign =false 
                    });

                }
                else{

                        $('#' + this.componentId).val('')
                        this.setState({ tree: tree})
                }
            }
            else {
                let modelData = this.props.model[this.props.property]
                if (!_.isEmpty(modelData)) {

                    let loadSign = false
                    for (let i = 0; i < modelData.length; i++) {
                        let ele = modelData[i]
                        // this.props.model[this.props.property].forEach(function (ele) {
                        //二次刷新状态
                                if (this.props.propSaveConentType == 'node') {
                            let theId = ele[this.props.jsonConfig.id]
                            let node = this.storeNodeMap.get(theId)
                            if (!node) {
                                loadSign = true
                                break;
                            }
                                }else{
                            let theId = ele
                            let node = this.storeNodeMap.get(theId)
                            if (!node) {
                                loadSign = true
                                break;
                            }
 
                                }
                    }
                    if (loadSign) {
                        this.testMount(tree, true)
                    }
                    else {

                        // this.setState({ tree })
                    }
                }

            }
        }

        // let bindModel = this.props.model
        // let bindProperty = this.props.property
        // let checkNodeIds = []
        // if (bindModel && bindProperty) {
        //     checkNodeIds = bindModel[bindProperty]
        //     if (checkNodeIds && tree.size > 0) {
        //     }
        //     this.setState({ tree })
        // }

    }


    createList(tree) {
        return this.convertArrayToTree(tree)
    }

    hashcode(str) {
        var hash = 0, i, chr, len;
        if (str.length === 0) return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }

    compareObjects(objA, objB) {
        return this.hashcode(JSON.stringify(objA)) == this.hashcode(JSON.stringify(objB))
    }

    componentDidMount() {

        // SelectUtil.setSelectComponentValue(this.state.tags.length, this.componentId);
        if (this.props.io != "out") {
            this.initValidator();
        }
    }

    componentWillMount() {
        super.componentWillMount();

        let msg = ""
        if (!this.props.data) {
            this.props.data = []
        }
        if (this.props.defaultValue) {

        }
        
    }

    componentDidMount() {
        this.loadESData(this.props.url, this.props, true);
    }

    getDefaultTags(ids) {
        let params = { "CodeTableName": this.props.CodeTableName, Ids: ids }

        AjaxUtil.call(this.props.getTagsUrl, params, { "method": "POST" }).then((data) => {

        });
    }


    componentDidUpdate() {
        if (Util.parseBool(this.props.needresetTags)) {
            _.each(this.tree, function(node) {
                node.checked =false
            }, this);
            if (!event || (event && event.target.className && event.target.className.indexOf('bootstrap-switch') != -1)) {
                this.loadESData(this.props.url, this.props, true);
            }
            let newDefaultTag = this.getModelTags([]);
            this.setState({tag:newDefaultTag});
            this.props.needresetTags = false
        }
        if (!Util.parseBool(this.props.readOnly)) {
            this.onCheckboxChange = this.onCheckboxChange.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
        this.onChangeColorByEndorsement();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.io == "out") {
            nextProps.readOnly = true
        }
    }


    onDrowdownHide() {
        if ($('#' + this.componentId).parents('div [needreposition=1]') && $('#' + this.componentId).parents('div [needreposition=1]').length > 0) {
            let w = $('#' + this.componentId).parents('td').attr('realwidth');

            $($('#' + this.componentId).parents('div [needreposition=1]')[0]).css({ position: 'relative', left: '0px', marginTop: '0px' });
        }

        this.searchInput.value = ''
        this.setState({ tree: this.tree, searchTreeMode: false })

        // needed when you click an item in tree and then click back in the input box.
        // react-simple-dropdown behavior is toggle since its single select only
        // but we want the drawer to remain open in this scenario as we support multi select
        if (this.keepDropdownActive) {
            this.dropdown.show()
        } else {
        }
    }

    test() {

    }

    contentOnShow() {
        if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
            let l = $('#for-input-' + this.componentId).position().left;
            let w = $('#' + this.componentId).parents('td').attr('realwidth');

            let curObjH = $('#for-input-' + this.componentId).height() / 2;
            $('#for-input-' + this.componentId).css({ position: 'absolute', left: l + 'px', marginTop: '-' + curObjH + 'px' });
        }

        $("#" + this.componentId + "-searchInput").focus()
    }

    onInputChange(value) {
        if (_.isEmpty(value)) {
            this.setState({ tree: this.tree, searchTreeMode: false })
        } else {

            let self = this
            let params = {
                "PageSize": 50,
                "PageNo": 0

            }
            params["SearchField"] = value
            params["CodeTableName"] = this.props.CodeTableName
            this.state.searchKey = value;
            // let tree = this.state.tree;
            // // tree.forEach(function (treeNode) {
            // //    treeNode.expanded = false
            // // }, this);
            // tree.forEach(function (treeNode) {
            //     if (treeNode[this.props.jsonConfig.label].indexOf(this.state.searchKey) != -1) {
            //         // treeNode.expanded = true
            //         this.onNodeToggle(treeNode)
            //     }
            // }, this);


            AjaxUtil.call(this.props.url, params, { "method": "POST" }).then((data) => {
                if (data) {
                    let tree = flattenTree(data.BusinessCodeTableValueList, self.props.jsonConfig, false, true)
                    tree.forEach(function (treeNode) {
                        let tempNode = this.tree.get(treeNode[this.props.jsonConfig.id])
                        if (tempNode) {
                            treeNode.checked = tempNode.checked
                        }
                        else {
                            treeNode.checked = false
                        }
                    }, this);
                    self.setState({ searchTree: tree, searchTreeMode: true })
                }
            });


        }
    }
    setStatePromise(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    async setStateASync(tree, searchModeOn, allNodesHidden, value) {

        await this.setState({
            tree, searchModeOn, allNodesHidden
        })

        $.each($('#' + this.componentId + ' .dropdown-content > div ul > li > label >span'), (index, obj) => {
            this.highlight(obj, value)
        })
    }



    onTagRemove(id, tar) {
        if (this.props) {
            this.onCheckboxChange(null, tar, false)
        }
    }

    reloadTree(node, params) {
        let preMap = new Map()
            let nextMap = new Map()
            let preSign = true
            this.tree.forEach(function (mapNode) {
                if (preSign) {
                    preMap.set(mapNode._id, mapNode)
                }
                else {
                    nextMap.set(mapNode._id, mapNode)
                }
                if (mapNode._id == node._id) {
                    preSign = false
                }
            }, this);

            AjaxUtil.call(this.props.url, params, { "method": "POST" }).then((data) => {
                if (!_.isEmpty(data)) {
                    data.BusinessCodeTableValueList.forEach(function (cnode) {
                        cnode._id = cnode[this.props.idKey]
                        cnode._parent = node._id
                        cnode._depth = node._depth + 1
                        cnode.hide = false
                        if (!node[this.props.jsonConfig.children]) {
                            node[this.props.jsonConfig.children] = []
                            preMap.set(cnode._id, cnode)
                            // this.tree.set(cnode._id, cnode)
                            node[this.props.jsonConfig.children].push(cnode[this.props.idKey])
                        }
                        else {

                            if (!this.tree[cnode._id]) {
                                preMap.set(cnode._id, cnode)
                                // this.tree.set(cnode._id, cnode)
                                node[this.props.jsonConfig.children].push(cnode[this.props.idKey])
                            }
                        }
                    }, this);

                    nextMap.forEach(function (node) {
                        preMap.set(node._id, node)
                    }, this);
                    this.tree = preMap


                }

                this.doCheckBoxChecked()

                this.setState({ tree: this.tree })

            });
    }
    onNodeToggle(node) {
        // node.expanded =!node.expanded

        //call api get data
        if (node["_noToggle"]) {
            return
        }

        node.expanded = !node.expanded

        let pid = node._id

        let params = {}
        params["CodeTableName"] = this.props.CodeTableName
        params["ConditionMap"] = {}
        params["ConditionMap"]["parentOrgId"] = pid
        params["PageSize"] = 500
        params["PageNo"] = 0

        let nodeChildren = node[this.props.jsonConfig.children];
        console.log('onNodeToggle...')
        if (_.isEmpty(nodeChildren)) {
            this.reloadTree(node, params);
        }
        else {
            if (node._depth != 0) {
                let children = node[this.props.jsonConfig.children]
                if (children) {
                    children.forEach(function (cnodeId) {
                        let cnode = this.tree.get(cnodeId)
                        this.treeNodesShowOrHidden(cnode, !Util.parseBool(node.expanded))
                    }, this);
                }

                this.doCheckBoxChecked()
                this.setState({ tree: this.tree });
            } else {
                this.reloadTree(node, params);
            }
        }
    }


    treeNodesShowOrHidden(pNode, status) {
        if (pNode == undefined || pNode == null) return;
        pNode.hide = status
        let children = pNode[this.props.jsonConfig.children]
        if (children) {
            if (status) {

                children.forEach(function (cnodeId) {
                    let cnode = this.tree.get(cnodeId)
                    this.treeNodesShowOrHidden(cnode, status)
                }, this);
            }
            else {
                pNode.expanded = false
            }
        }
    }



    onCheckboxChange(event, node, checked) {
        node.checked = checked
        if (!Util.parseBool(this.props.readOnly)) {

            let tags = []
            // 单选
            if (Util.parseBool(this.props.singleSelect)) {
                this.storeNodeMap = new Map();
                this.setState({
                    tags: []
                });
                this.tree.forEach(function (mapNode) {
                   mapNode.checked = false;
                }, this);
            }
            if (checked) {
                this.storeNodeMap.set(node._id, _.clone(node))

            }
            else {
                this.storeNodeMap.delete(node._id)
                if (this.state.searchTreeMode) {
                    let cnode = this.state.searchTree.get(node._id)
                    cnode.checked = false
                } else {
                    let cnode = this.tree.get(node._id)
                    if (cnode) {
                        cnode.checked = false
                    }

                }
            }
            this.storeNodeMap.forEach(function (node) {
                tags.push(node)
                let tnode = this.tree.get(node[this.props.jsonConfig.id])
                if (tnode) {
                    tnode.checked = true
                }
            }, this);
            this.props.refreshSign =false
            this.setState({
                tags: tags, tree: this.tree
            })
            if (this.props.model && this.props.property) {
                if (this.props.propSaveConentType == 'node') {

                    this.props.model[this.props.property] = _.clone(tags);
                }
                else {
                    this.props.model[this.props.property] = []
                    tags.forEach(function (element) {

                        this.props.model[this.props.property].push(element._id)
                    }, this);
                }


            }
            // if(tags.length >0){
            //     $('#' + this.componentId).val('hasValue')

            // } else {
            //     $('#' + this.componentId).val('')
            // }

            this.notifyChange(node, checked, tags);
            this.onChangeColorByEndorsement();//endorsement color

        }
    }

    onAction = (actionId, nodeId) => {
        // typeof this.props.onAction === 'function' && this.props.onAction(actionId, this.treeManager.getNodeById(nodeId))
    }

    copyToClipboard(text) {
        // IE specific
        if (window.clipboardData && window.clipboardData.setData) {
            return clipboardData.setData("Text", text);
        }

        // all other modern
        let target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.textContent = text;
        document.body.appendChild(target);
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection of fall back to prompt
        try {
            document.execCommand("copy");
            target.remove();
        } catch (e) {
            console.log("Can't copy string on this browser. Try to use Chrome, Firefox or Opera.")
            window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
        }
    }

    copyLabel(event, value) {
        let label = this.props.jsonConfig.label
        event.preventDefault()
        let result = []
        this.state.tags.forEach(element => {
            result.push(element[label])
        });
        this.copyToClipboard(result.join(this.props.copySplitFlag))

    }

    highlight(ele, keyword) {
        var textbox = ele;
        var regexClear = /(<([^>]+)>)/ig
        var temp = textbox.innerHTML.replace(regexClear, '');

        if ("" == keyword) {
            textbox.innerHTML = temp
            return;
        }

        let pinyinIndex = $(ele).attr("pinyin")
        //获取所有文字内容，并清空之前被高亮的标签
        var htmlReg = new RegExp("\<.*?\>", "i");
        var arr = new Array();

        //替换HTML标签 
        for (var i = 0; true; i++) {
            //匹配html标签 
            var tag = htmlReg.exec(temp);
            if (tag) {
                arr[i] = tag;
            } else {
                break;
            }
            temp = temp.replace(tag, "{[(" + i + ")]}");
        }

        if (pinyinIndex >= 0) {
            let highlightWords = temp.substr(pinyinIndex, keyword.length)
            temp = temp.replace(highlightWords, "<b style='color:Red;'>" + highlightWords + "</b>")
        }

        textbox.innerHTML = temp;
    }


    convertArrayToTree(arrayData) {
        let uniqueArray = Array.from(new Set(arrayData))
        let rootNode = this.props.rootNode;
        if (null != rootNode) {
            this.getChild(uniqueArray, rootNode)
        }
        return rootNode

    }


    getChild(arrayData, rootNode) {
        let endIndex = this.props.parentKeyPath.length - 1;
        if (arrayData.length > 0) {
            arrayData.forEach(function (node) {
                let parentId = this.getLookupParentValue(node, 0, this.props.parentKeyPath, endIndex, this)
                if (parentId == rootNode[this.props.idKey]) {
                    if (rootNode[this.props.jsonConfig.children] == null) {
                        rootNode[this.props.jsonConfig.children] = []
                    }
                    rootNode[this.props.jsonConfig.children].push(node)
                    this.getChild(arrayData, node)
                }
            }, this);
        } else {
            rootNode[this.props.jsonConfig.children].push(this.props.rootNode);
        }
    }

    getLookupParentValue(value, index, keyPathArr, endIndex, self) {
        value = value[keyPathArr[index]]
        index += 1
        if (index <= endIndex) {
            value = self.getLookupParentValue(value, index, keyPathArr, endIndex, self)
        }
        return value
    }


    doCheckBoxChecked() {
        if (this.storeNodeMap.size > 0) {
            let keys = [...this.storeNodeMap.keys()]
            keys.forEach(function (chkId) {
                if (this.tree.get(chkId)) {
                    this.tree.get(chkId).checked = true
                }
            }, this);
        }
    }

    getModelTags(tags) {
        let bindModel = this.props.model
        let bindProperty = this.props.property
        let checkNodeIds = [];
        let _self = this;

        if (tags.length == 0 && bindModel && bindProperty) {

            //todo:  这里需要根据对象或者ID来修改
            //点:接收参数的时候也要
            //考虑jsonConfig的内容变动
            checkNodeIds = bindModel[bindProperty]
           
            let tags2 = [];
            if (Util.parseBool(this.props.needresetTags)) {
                tags2 = this.state.tags;
                return tags2;
            }
            if (checkNodeIds.length > 0 && _self.tree) {
                checkNodeIds.forEach(function(id) {
                    _self.tree.forEach(function (treeNode) {
                        let tempNode = _self.tree.get(treeNode[_self.props.jsonConfig.id])
                        if (tempNode && tempNode[_self.props.jsonConfig.id] == id) {
                            treeNode.checked = true;
                            tags2.push(treeNode);
                            _self.storeNodeMap.set(treeNode._id, _.clone(treeNode));
                        }
                    }, this);
    
                });
                if (tags2.length > 0 && !Util.parseBool(this.props.needresetTags)) {
                    this.setState({tags: tags2});
                }
            }
            return tags2;
        }
        return tags;
    }
    renderInputComponent() {
        let { jsonConfig } = this.props
        let triggerStyle = {}
        if (this.props.inputWidth) {
            triggerStyle.width = this.props.inputWidth
        }
        else {
            triggerStyle.width = "100%"
        }
        if (this.props.io == "out") {
            triggerStyle.width = "100%"
            triggerStyle.border = "none"
        } else {
            if (!this.props.validator) {
                if (!Util.parseBool(this.props.required)) {
                    this.clearValidationInfo()
                    ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);                    
                } else {
                    this.initValidator();
                }
            }
        }
        return (
            this.props.io == 'in' ?
            <div className={cn(this.props.className, 'react-dropdown-tree-select', 'input-group')} disabled={this.props.enabledDrop} id={this.componentId} name={this.getName()} required={this.props.required}>
                <Dropdown disabled={this.props.enabledDrop} enabledDrop={this.props.enabledDrop} disabled={false} ref={el => { this.dropdown = el }} onHide={this.onDrowdownHide} onShow={this.contentOnShow.bind(this)}>
                    <DropdownTrigger className={cx('dropdown-trigger')} style={triggerStyle}>
                        <TreeInput
                            inputRef={el => { this.searchInput = el }}
                            enabledDrop={this.props.enabledDrop}
                            tags={this.getModelTags(this.state.tags)}
                            placeholderText={this.props.placeholderText}
                            onInputChange={this.onInputChange.bind(this)}
                            onFocus={() => { this.keepDropdownActive = true }}
                            onBlur={() => { this.keepDropdownActive = false }}
                            onTagRemove={this.onTagRemove}
                            mostVisableItem={this.props.mostVisableItem}
                            perItemMaxLength={this.props.perItemMaxLength}
                            labellabel={jsonConfig.label}
                            onCopy={this.copyLabel.bind(this)}
                            pContainerId={this.componentId}
                            io={this.props.io}
                        />
                        <span onClick={this.test.bind(this)}></span>
                    </DropdownTrigger>
                    <DropdownContent className={cx('dropdown-content')} style={this.props.contentWidth ? { width: this.props.contentWidth } : { "width": "100%" }}>
                        <div>
                            {this.state.allNodesHidden
                                ? <span className='no-matches'>No matches found</span>
                                : (
                                    <Tree data={this.state.searchTreeMode ? this.state.searchTree : this.state.tree}
                                        searchModeOn={this.state.searchModeOn}
                                        onAction={this.onAction}
                                        onCheckboxChange={this.onCheckboxChange.bind(this)}
                                        searchKey={this.state.searchKey}
                                        onNodeToggle={this.onNodeToggle.bind(this)} jsonConfig={jsonConfig} />

                                )
                            }
                        </div>

                    </DropdownContent>
                </Dropdown>
            </div>
            :
            <TreeInput
                            inputRef={el => { this.searchInput = el }}
                            enabledDrop={this.props.enabledDrop}
                            tags={this.state.tags}
                            placeholderText={this.props.placeholderText}
                            onInputChange={this.onInputChange.bind(this)}
                            onFocus={() => { this.keepDropdownActive = true }}
                            onBlur={() => { this.keepDropdownActive = false }}
                            onTagRemove={this.onTagRemove}
                            mostVisableItem={this.props.mostVisableItem}
                            perItemMaxLength={this.props.perItemMaxLength}
                            labellabel={jsonConfig.label}
                            onCopy={this.copyLabel.bind(this)}
                            pContainerId={this.componentId}
                            io={this.props.io}
                            needellipsis={this.props.needellipsis}
                        />
        )
    }
}



/**
 * InputTreeSelect component prop types
 */
InputEsTreeSelect.propTypes = $.extend({}, UIInput.propTypes, {
    data: PropTypes.array.isRequired,
    url: PropTypes.string.isRequired,
    getTagsUrl: PropTypes.string,
    placeholderText: PropTypes.string,
    showDropdown: PropTypes.bool,
    className: PropTypes.string,
    onChange: PropTypes.func,
    onCheckboxChange: PropTypes.func,
    // onAction: PropTypes.func,
    // onNodeToggle: PropTypes.func,
    defaultValue: PropTypes.array,
    readOnly: PropTypes.bool,
    singleSelect: PropTypes.bool,
    mostVisableItem: PropTypes.number,
    copySplitFlag: PropTypes.string,
    perItemMaxLength: PropTypes.number,
    inputWidth: PropTypes.string,
    contentWidth: PropTypes.string,
    noCheckParent: PropTypes.bool,
    searchHighLight: PropTypes.bool,
    showParentSplitSign: PropTypes.string,
    propSaveConentType: PropTypes.oneOf(["id", "node"]),
    jsonConfig: PropTypes.shape({
        id: PropTypes.string,
        children: PropTypes.string,
        label: PropTypes.string
    }),
    overflowMarginTop: PropTypes.string,
    displayByOrder: PropTypes.bool,
    parentKeyPath: PropTypes.array,
    idKey: PropTypes.string,
    rootNode: PropTypes.object,
    enabledDrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    needellipsis: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    needCompare: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    needresetTags: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    CodeTableName: PropTypes.string
})

/**
 * Get InputTreeSelect component default props
 */
InputEsTreeSelect.defaultProps = $.extend({}, UIInput.defaultProps, {
    readOnly: false,
    mostVisableItem: 3,
    copySplitFlag: ",",
    perItemMaxLength: 8,
    defaultValue: [],
    noCheckParent: false,
    propSaveConentType: "id",
    searchHighLight: false,
    singleSelect: false,
    componentType: 'InputTreeSelect',
    jsonConfig: {
        id: "id",
        children: "children",
        label: "label"
    },
    parentKeyPath: ["OtherParams", "parentOrgId"],
    idKey: "id",
    overflowMarginTop: '-14px',
    displayByOrder: false,
    CodeTableName: 'branch',
    needellipsis: true,
    enabledDrop: false, // 可以展开下拉
    needCompare: false,  // 要不要每次跳过比较
    needresetTags: false // 其它组件更新时，重新设置tags
});